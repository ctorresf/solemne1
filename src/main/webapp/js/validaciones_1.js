function fnValidaDatos() {
    error = "";
    nombreCliente = document.getElementById("txtClient").value;
    edadCliente = document.getElementById("txtAge").value;
    montoInvertir = document.getElementById("txtAmount").value;
    cantidadAños = document.getElementById("lstYears").value;
    if (nombreCliente == "") {
        error = error + "Debe ingresar el nombre del cliente\n";
    }
    if (edadCliente == "") {
        error = error + "Debe ingresar la edad del cliente\n";
    } else {
        if (isNaN(edadCliente)) {
            error = error + "La edad del cliente no es válida\n";
        } else {
            if (parseInt(edadCliente) < 18 || parseInt(edadCliente) > 75) {
                error = error + "No cumple con la Edad requerida para invertir\n";
            }
        }
    }
    if (montoInvertir == "") {
        error = error + "Debe ingresar el monto a invertir\n";
    } else {
        if (isNaN(montoInvertir)) {
            error = error + "El monto ingresado no es válido\n";
        }
    }
    if (cantidadAños == "") {
        error = error + "Debe seleccionar la cantidad de años para su inversión\n";
    }
    if (error != "") {
        alert("Han ocurrido los siguientes errores:\n\n" + error);
    } else {
        document.frmInteres.submit();
    }
}

function fnLimpiar() {
    document.getElementById("txtClient").value = "";
    document.getElementById("txtAge").value = "";
    document.getElementById("txtAmount").value = "";
    document.getElementById("lstYears").value = "";
}
