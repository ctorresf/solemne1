<%@page import="modelo.InteresModelo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% InteresModelo calculadoraInteres = (InteresModelo) request.getAttribute("calculadoraIntereses");
    if (calculadoraInteres.getNombreCliente() == "") {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Formulario Solicitud Crédito Hipotecario</title>
        <script type="text/javascript" src="js/validaciones.js"></script>
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
    </head>
    <body>
        <form name="frmInteres" id="frmInteres">
            <table border="0" cellspacing="2" cellpadding="2" width="650" align="center">
                <tr>
                    <td align="center">
                        <fieldset><legend>Resultado Inversión</legend>
                            <table align="center" cellpadding="2" cellspacing="1" border="0" widht="500">
                                <tr>
                                    <td align="center" colspan="2" >
                                        <span style="font-size: 18pt; font-family: roboto; font-weight: bold">
                                            Estimado(a) <%= calculadoraInteres.getNombreCliente()%>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table border="0">
                                <tr align="center">
                                    <td>
                                        <span style="font-size: 16pt; font-family: roboto">
                                            De acuerdo a la información entregada, su inversión inicial por $ <%= calculadoraInteres.getMontoInvertir()%>, y a <%= calculadoraInteres.getAñosSolicitud()%> año/s, con una tasa del 1.5%, ha producido un interés de $ <%= calculadoraInteres.getCalculoInteres()%>, por lo que sus haberes asienden a un total de <span style="color: red; font-weight: bold">$ <%= calculadoraInteres.getResultadoFinal()%></span>.
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <table border="0">
                                <tr>
                                    <td align="center" colspan="2" style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px;">
                                        <input type="button" name="btnVolver" id="btnVolver" value="Volver" class="boton" tabindex="1" onclick="window.location.href = 'index.jsp';">
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
